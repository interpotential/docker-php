FROM php:5.6-apache

RUN set -eux; \
	apt-get update; \
	apt-get install --yes locales geoip-database geoip-database-extra; \
	sed -i -re 's/# (en_GB)/\1/' /etc/locale.gen; \
	locale-gen; \
	echo "Europe/Amsterdam" > /etc/timezone; \
	dpkg-reconfigure -f noninteractive locales; \
	dpkg-reconfigure -f noninteractive tzdata; \
	echo ok;

ENV LANGUAGE=en_GB.UTF-8 LANG=en_GB.UTF-8 LC_ALL=en_GB.UTF-8
ENV PHP_EXT_DEPS \
		libgeoip-dev  \
		libmemcached-dev \
		libssh2-1-dev \
		libc-client-dev libkrb5-dev \
		libxslt1-dev \
		libcurl4-gnutls-dev \ 
		zip \
		zlib1g-dev \
		git \
		ssh-client \
		apt-transport-https \
		lsb-release \
		gawk \
		libyaml-dev \
		libsqlite3-dev \
		sqlite3 \
		libgmp-dev \
		libgdbm-dev \
		libncurses5-dev \
		automake \
		libfreetype6-dev \
		libjpeg62-turbo-dev \
		libmcrypt-dev \
		libpng-dev \
		ruby-full \
		libtool \
		bison \
		libffi-dev \
		libgmp-dev \
		libreadline6-dev \
		libssl-dev

RUN set -eux; \
	apt-get install --yes $PHP_EXT_DEPS; \
	docker-php-ext-configure gd --with-freetype-dir=/usr/include/ --with-jpeg-dir=/usr/include/; \
	docker-php-ext-configure imap --with-kerberos --with-imap-ssl; \
	docker-php-ext-install -j$(nproc) \
		zip iconv mcrypt gd imap gettext xsl curl bcmath mysql exif pdo_mysql; \
	pecl install geoip; \
	pecl install memcached-2.2.0 --with-libmemcached-dir=no; \
	pecl install ssh2; \
	docker-php-ext-enable memcached geoip ssh2

RUN apt-get --yes install wget curl vim
RUN curl -sL https://deb.nodesource.com/setup_8.x | bash -
RUN wget https://raw.githubusercontent.com/composer/getcomposer.org/master/web/installer -O - -q | php -- --quiet --install-dir /usr/local/bin
RUN apt-get --yes install nodejs

RUN a2enmod rewrite headers expires
